# README #

### What is this repository for? ###

* It is just evaluation task. Application shows Github's top repositories sorted by stars with number of stars, watchers and open issues with possibility to search by repo name.


*** Task

Create app using React + react-router + redux or React Native  + redux.

**** Requirements

* Use Github Rest API https://developer.github.com/v3/


* On main screen of app show top repositories sorted by stars with number of stars, watchers and open issues.


* On top of page should be search input with possibility to search by repo name (sort by stars).


* When pressing on repository item show additional repo information with list of the last 10 pull requests with author, name, number and status.



On google play:

[![en_badge_web_generic.png](https://bitbucket.org/repo/9pKMbE6/images/3446325083-en_badge_web_generic.png)](https://play.google.com/store/apps/details?id=com.fundashboard&rdid=com.fundashboard)