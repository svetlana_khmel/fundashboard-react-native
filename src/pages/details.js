import React, { Component } from 'react';
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    Dimensions
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as dataActions from "../actions/data";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Details extends Component {

    componentDidMount () {
        this.props.fetchActive(this.props.user, this.props.repo);
    }

   render() {
        if(this.props.info.length > 0) {

            var rows = [];

            for (let index = 0; index < 10; index++) {
                rows.push (
                    <View style={styles.container} key={'row-' + index}>
                          <Text style={styles.repo}>Name: {this.props.info[index].user.login}</Text>
                          <Text style={styles.number}>Number: {this.props.info[index].number}</Text>
                          <Text style={styles.name}>Owner: {this.props.info[index].base.repo.owner.login}</Text>
                    </View>
                );
            }

            return (
               <ScrollView>{rows}</ScrollView>
            )
        } else {
           return(
               <View style={styles.loadingView}><Text style={styles.loading}>Loading...</Text></View>
           );
         }
   }
}

function mapStateToProps (state) {
    return {
        user: state.setUser.data.activeUser,
        repo: state.setUser.data.repo,
        info: state.setData?state.setData.loaded : []
    }
}

export default connect(
    mapStateToProps,
    dispatch => bindActionCreators(dataActions, dispatch)
)(Details);

const styles = StyleSheet.create({
container: {
        paddingTop: 60,
        flex: 1,
        flexDirection: 'column'
    },
    loadingView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center'
    },
    loading: {
        fontSize: 20,
        color: '#666'
    },
    repo: {
        fontSize: 20,
        textAlign: 'center',
        margin: 0,
        color: '#1b1a16'
    },
    number: {
        fontSize: 10,
        textAlign: 'center',
        margin: 5,
        color: '#e8c70e'
    },
    name: {
        fontSize: 10,
        textAlign: 'center',
        margin: 0,
        color: '#579c0a'
    }
});

