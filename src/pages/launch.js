import React, { Component } from 'react';
import {
    View,
    ScrollView,
    Text,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import * as dataActions from "../actions/data";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Search from '../components/search';

class Launch extends Component {
    constructor() {
        super();

        this.loadData = this.loadData.bind(this);
        this.renderRows = this.renderRows.bind(this);
        this.doSearch = this.doSearch.bind(this);

        this.state = {
          data: null,
          query: '',
          filteredData: null,
          activeUser: null
        };
    }

    componentWillMount () {
        this.loadData();
    }

   loadData () {

        this.props.fetchData().then(() => {

            this.setState({
                data: this.props.data.items,
                filteredData: this.props.data.items
            });
        });
   }

    doSearch (e) {
        let queryResult = [];
        let queryText = e.nativeEvent.text;

        this.state.data.forEach(function (person) {
            if(person.name.toLowerCase().indexOf(queryText.toLowerCase())!=-1)
            queryResult.push(person);
            console.log(person.name);
        });

        this.setState({
            query: queryText,
            filteredData: queryResult
        });
    }

    moreDetails (owner, repo) {
        let data = {
            activeUser: owner,
            repo
        }

        this.setState(data);
        this.props.setActive(data);
        Actions.details();
    }

    renderRows() {
        var rows = [];

        for (let i = 0; i < this.state.filteredData.length; i++) {
            rows.push(
                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} key={this.state.data[i].id} onPress={()=>this.moreDetails(this.state.filteredData[i].owner.login, this.state.filteredData[i].name)}>
                    <Text style={styles.name}>{this.state.filteredData[i].name}</Text>
                    <Text style={styles.stars}>Stars: {this.state.filteredData[i].watchers_count}</Text>
                    <Text style={styles.watchers}>Watchers: {this.state.filteredData[i].watchers}</Text>
                    <Text style={styles.issues}>Issues: {this.state.filteredData[i].open_issues}</Text>
                </TouchableOpacity>
            );
        }

        return (
            <ScrollView>
              {rows}
            </ScrollView>
         )
     }

     render() {
          if(this.state.data === null) {
              return(
                  <View style={styles.loadingView}><Text style={styles.loading}>Loading...</Text></View>
              );
          }

          return(
            <View>
               <Search doSearch={this.doSearch} query={this.state.query} />

              {this.renderRows()}
            </View>
          );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.dataReducer?state.dataReducer.data:{},
        user: state.activeUser
    };
};

export default connect(
    mapStateToProps,
    dispatch => bindActionCreators(dataActions, dispatch)
)(Launch);

const styles = StyleSheet.create({
    test: {
        margin: 50,
        padding: 100
    },
    loadingView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center'
    },
    loading: {
        fontSize: 20,
        color: '#666'
    },
    name: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#1b1a16'
    },
    stars: {
        fontSize: 10,
        textAlign: 'center',
        margin: 10,
        color: '#e8c70e',
        marginTop: 15
    },
    watchers: {
        fontSize: 10,
        textAlign: 'center',
        margin: 10,
        color: '#579c0a',
        marginTop: 15
    },
    issues: {
        fontSize: 10,
        textAlign: 'center',
        margin: 10,
        color: '#de0d0d',
        marginTop: 15
    }
});

