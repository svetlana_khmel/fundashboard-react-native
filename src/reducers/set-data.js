export default function (state=null, action) {
    switch (action.type) {
         case 'SET_ACTIVE_DATA':
            return {
                ...state,
                loaded: action.payload
            }

        default:
        return state
    }
}