export default function (state = null, action) {
    switch (action.type) {
        case 'DATA_LOADED':
            return {
                ...state,
                data : action.payload
            };

        default:
        return state
    }

}