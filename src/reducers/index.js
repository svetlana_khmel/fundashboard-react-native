import { combineReducers } from 'redux';
import dataReducer from './data-reducer';
import setUser from './set-user';
import setData from './set-data';

const rootReducer = combineReducers({
    dataReducer,
    setUser,
    setData
});

export default rootReducer;