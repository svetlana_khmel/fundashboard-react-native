import React, { Component } from 'react';
import { Text, View, TextInput } from 'react-native';

class Search extends Component {
    constructor() {
        super();
        this.doSearch = this.doSearch.bind(this);
  }

  doSearch (e) {
      this.props.doSearch(e.target.value);
  }

  render () {
    return (
        <View>
          <TextInput
                style={{marginTop: 100, height: 40, borderColor: 'gray', borderWidth: 1}}
                onChange={this.props.doSearch}
                value={this.props.query}
            />
        </View>
      );
  }
};

const styles = {
  viewStyle: {
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    paddingTop: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 20
  }
};

export default Search;
