import data from '../data/data';

function setData(data) {
    return {
        type: 'DATA_LOADED',
        payload: data
    }
}

export function setActive (data) {
    return {
        type: 'SET_USER',
        payload: data
    }
}

export function setActiveData (data) {
    return {
        type: 'SET_ACTIVE_DATA',
        payload: data
    }
}

export function fetchData() {
    return function (dispatch, getState) {
        return data.getData()
            .then((response) => response.json())
            .then((data) => dispatch(setData(data)))
            .catch((err) => console.log(err));
    };
};

export function fetchActive(user, repo) {
    return function (dispatch, getState) {
        return data.getActiveData(user, repo)
            .then((response) => response.json())
            .then((data) => dispatch(setActiveData(data)))
            .catch((err) => console.log(err));
    };
};

