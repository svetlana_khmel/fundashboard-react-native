const dataUrl = 'https://api.github.com/search/repositories?q=stars:>1000&sort=stars&order=desc';
const baseURL = 'https://api.github.com/repos/';
const params = 'pulls?state=all';

//'https://api.github.com/repos/'+ this.props.user +'/'+ this.props.repo +'/pulls/10';

const data = {
    getData () {
        return fetch(dataUrl);
    },

    getActiveData (user, repo) {
        return fetch(`${baseURL}${user}/${repo}/${params}`);
    }
}

export default data;