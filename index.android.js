import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  ListView,
  WebView,
  Switch,
  Dimensions,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  Platform,
  Picker
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux';
import Launch from './src/pages/launch';
import Details from './src/pages/details';
//import Header from './src/components/header'; 
//import InputText from './src/components/inputText';

import { Provider } from 'react-redux';
import configureStore from './src/configureStore';

const store = configureStore();

export default class DashBoard extends Component {

constructor() {
    super();
  }

  render() {
      return (
      <Provider store={store}>
           <Router>
             <Scene key="root">
               <Scene key="launch" component={Launch} title="Launch" initial={true} />
               <Scene key="details" component={Details} title="Details"/>
             </Scene>
           </Router>
       </Provider>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('fundashboard', () => DashBoard);
